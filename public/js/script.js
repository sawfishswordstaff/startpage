/**
 * Web app metadata
 */
const METADATA = {
    VERSION: Object.freeze("1.0.0")
};

/**
 * Entry point
 */
const main = () => {
    const SEARCHBAR = Object.freeze(document.querySelector("#searchbar"));
    const VERSION_DISPLAY = Object.freeze(document.querySelector("#version"));

    // search bar element
    if (SEARCHBAR)
    {
        SEARCHBAR.addEventListener("keyup", e => {
            // grab search bar content
            const term = e.target.value;

            if (e.code === "Enter")
                window.location.href = "https://duckduckgo.com/?q=" + term;
        });
    }

    // version element
    if (VERSION_DISPLAY)
        VERSION_DISPLAY.textContent = METADATA.VERSION;
};

main();
